package Controlador;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class CalculadoraServlet
 */
public class CalculadoraServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CalculadoraServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		// doGet(request, response);
		
		// Start writing here
		
		response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {

            //Recuperación de datos desde el formulario
            //en el getparameter va el nombre del name del campo
            double n1 = Double.parseDouble(request.getParameter("n1"));
            double n2 = Double.parseDouble(request.getParameter("n2"));
            double salida = 0;
            int opt = Integer.parseInt(request.getParameter("opt"));

            //Procesar --- Puedo delegar la responsabilidad o hacerlo yo
            
            switch (opt) {
            	 case 0:
            		 salida = n1 + n2;
            		 break;
            	 case 1:
            		 salida = n1 - n2;
            		 break;
            	 case 2:
            		 if (n2 == 0) {
            			 salida = -1;
            		 } else {
            			 salida = n1 / n2;
            		 }
            		 break;
            	 case 3:
            		 salida = n1 * n2;
            		 break;
            	 default:
            		 salida = -1;
            }
            
            //pintar valores en el formulario
            request.getSession().setAttribute("resultado", salida);
            
            response.sendRedirect("index.jsp");response.setContentType("text/html;charset=UTF-8");
		
        }

	}
}