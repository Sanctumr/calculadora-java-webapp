<!DOCTYPE html>
<html lang="es">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<title>Calculadora</title>
		
		<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css"
        integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous"/>
    	<script defer src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
        integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj"
        crossorigin="anonymous"></script>
    	<script defer src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx"
        crossorigin="anonymous"></script>
        <link rel="stylesheet" href="css/style.css">
	</head>
	<body>
		<main>
			<div class="container" style="background-color: #fafafa">
            <div class="row justify-content-center">
                <div class="col-md-6">
                    <form action="CalculadoraServlet" method="POST">
                        <h1 class="titulo">Calculadora JSP</h1>
                        <div class="form-group">
                            <label for="n1">N�mero 1</label>
                            <input type="text" class="form-control" id="n1" name="n1"/>
                        </div>
                        <div class="form-group">
                            <label for="n2">N�mero 2</label>
                            <input type="text" class="form-control" id="n2" name="n2"/>
                        </div>
                        <div>
                        	<label for="operation">Operaci�n</label>
                        </div>
                        <div class="form-check form-check-inline">
                        	<input class="form-check-input" type="radio" id="operation" name="opt" value="0" checked>
                        	<label class="form-check-label">Suma</label>
                        </div>
                        <div class="form-check form-check-inline">
                        	<input class="form-check-input" type="radio" id="operation" name="opt" value="1">
                        	<label class="form-check-label">Resta</label>
                        </div>
                        <div class="form-check form-check-inline">
                        	<input class="form-check-input" type="radio" id="operation" name="opt" value="2">
                        	<label class="form-check-label">Divisi�n</label>
                        </div>
                        <div class="form-check form-check-inline">
                        	<input class="form-check-input" type="radio" id="operation" name="opt" value="3">
                        	<label class="form-check-label">Multiplicaci�n</label>
                        </div>
                        <div>
                        	<br>
                        	<button type="submit" class="btn btn-info" id="calcular" name="btnLimpiar">Enviar</button>
                        </div>

                        <div class="form-group">
                        	<br>
                            <label for="calcular">Resultado</label>
                            <input type="text" class="form-control" id="resultado" disabled name="resultado"
                            value="<%
                            	out.print(request.getSession().getAttribute("resultado")==null?"":request.getSession().getAttribute("resultado"));
                            	%>"/>
                        </div>
                    </form>
                </div>
            </div>
        </div>
		</main>
	</body>
</html>
